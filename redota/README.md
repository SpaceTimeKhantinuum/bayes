# redota

javascript reply parser and awesome browser replay viewer

issue: [how to use](https://github.com/timkurvers/redota/issues/56)

```
$ conda create -n redota python=3.9
$ conda activate redota
$ conda install -c conda-forge nodejs==16.13
```

clone https://github.com/timkurvers/redota in this directory
because the script in the dir `test-js` expects it

need nodejs version 16 at the time or writting.
see [issue](https://github.com/timkurvers/redota/issues/56#issuecomment-989282800)



```
(redota) redota $ ./bin/parse.js ~/Downloads/6173366796_1474179147.dem
(node:7255) ExperimentalWarning: Importing JSON modules is an experimental feature. This feature could change at any time
(Use `node --trace-warnings ...` to show where the warning was created)
summary CDemoFileInfo {
  playbackTime: 4819.00048828125,
  playbackTicks: 144570,
  playbackFrames: 72281,
  gameInfo: CGameInfo {
    dota: CDotaGameInfo {
      playerInfo: [Array],
      picksBans: [Array],
      matchId: [Long],
      gameMode: 2,
      gameWinner: 3,
      leagueid: 13503,
      radiantTeamId: 8291895,
      direTeamId: 7119388,
      radiantTeamTag: 'Tundra',
      direTeamTag: 'TSpirit',
      endTime: 1631212831
    }
  }
}
header CDemoFileHeader {
  demoFileStamp: 'PBDEMS2\x00',
  networkProtocol: 47,
  serverName: 'Valve Dota 2 Europe Server (srcds407-fra2.274.189)',
  clientName: 'SourceTV Demo',
  mapName: 'start',
  gameDirectory: '/opt/srcds/dota/dota_v4994/dota',
  fullpacketsVersion: 2,
  allowClientsideEntities: true,
  allowClientsideParticles: true,
  addons: '',
  demoVersionName: 'valve_demo_2',
  demoVersionGuid: '8e9d71ab-04a1-4c01-bb61-acfede27c046',
  buildNum: 9028
}
# of entities at replay start: 86

# of entities at replay end: 1135
time taken: 10.805s
```

in `test-js` dir

run

```
$ node --experimental-modules --experimental-json-modules replay-test.mjs
```


note the `.mjs` extension and the flags to `node`.

These are there to fix the following error

```
import fs from 'fs';
^^^^^^

SyntaxError: Cannot use import statement outside a module.
This [link](https://exerror.com/syntaxerror-cannot-use-import-statement-outside-a-module-in-nodejs/)
shows you how to fix it.






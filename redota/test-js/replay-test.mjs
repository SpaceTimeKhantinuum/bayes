

import fs from 'fs';

import Replay from '../redota/src/lib/replay/Replay.js';
import Parser from '../redota/src/lib/parser/Parser.js';

import { GAME_PHASE } from '../redota/src/lib/constants.js';


// Full length parse of given replay file for debugging purposes
const buffer = fs.readFileSync("/Users/sebastian.khan/Downloads/6173366796_1474179147.dem");


const replay = new Replay(buffer);
replay.start();

// Jumps to the start of the game
replay.jumpTo(GAME_PHASE.START);
//replay.jumpTo(5);

// Seeks 1000 ticks forward
replay.seek(replay.tick + 1000);

for (const player of replay.players) {
  if (!player.isPlayer) continue;

  const { relX, relY } = player.hero.position;

  console.log(player.name, player.hero.name, relX, relY);
}

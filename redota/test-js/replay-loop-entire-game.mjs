// how to run
//(redota) test-js $ node --experimental-modules --experimental-json-modules replay-loop-entire-game.mjs

import fs from 'fs';

import Replay from '../redota/src/lib/replay/Replay.js';

import { GAME_PHASE } from '../redota/src/lib/constants.js';

// Full length parse of given replay file for debugging purposes
const buffer = fs.readFileSync("/Users/sebastian.khan/Downloads/6173366796_1474179147.dem");

// load the replay once to get last tick
// must be a better way to do this
const replay0 = new Replay(buffer);
replay0.start();
replay0.jumpTo(GAME_PHASE.END);
const last_tick = replay0.tick;
console.log("last_tick");
console.log(last_tick);

// load replay from beginning
const replay = new Replay(buffer);
replay.start();
// Jumps to the start of the game
replay.jumpTo(GAME_PHASE.START);


// array to store player objects
const players = [];

// find which parts of replay.players are actually payers
let i = 0;
for (const player of replay.players) {
  if (!player.isPlayer) continue;
  players[i] = player;
  i++;
//   const { relX, relY } = player.hero.position;
//   console.log(player.name, player.hero.name, relX, relY);
}

// now loop over the ticks of the game
// and use the created players array to get the player
// positions
var positions = new Array ( );
let j = 0;
// const N_heroes = 2;
const N_heroes = 10;
console.log("creating positions array")
for (j = 0; j < N_heroes; j++){
  console.log(j)
  positions[j] = new Array ();
}
i = 0;
while (replay.tick < last_tick) {
// while (replay.tick < 35081) {
    // Seeks 1000 ticks forward
    replay.seek(replay.tick + 100);

    // j look is over heros
    for (j = 0; j < N_heroes; j++){
      const { relX, relY, x, y } = players[j].hero.position;
      positions[j][i] = [replay.tick, relX, relY, x, y, players[j].hero.name];
    }

    i++;
    // console.log(replay.tick, last_tick, relX, relY);
}

// console.log(positions[0])
// console.log(positions[1])
// console.log(positions)

// https://stackoverflow.com/questions/17614123/node-js-how-to-write-an-array-to-file

for (j = 0; j < N_heroes; j++){
  console.log("writing array " + j)
  var file = fs.createWriteStream('array_' + j + '.txt');
  file.on('error', function(err) { /* error handling */ });
  positions[j].forEach(function(v) { file.write(v.join(', ') + '\n'); });
  file.end();  
}

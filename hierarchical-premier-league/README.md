# Description

Learning how to implement a Bayesian hierarchical model
based on [this Rstan tutorial](https://www.youtube.com/watch?v=dNZQrcAjgXQ)

Uses the model based on [Bayesian hierarchical model for the prediction of football results](https://discovery.ucl.ac.uk/id/eprint/16040/1/16040.pdf)

Also using [pymc:rugby_analytics.html#Building-of-the-model](https://docs.pymc.io/notebooks/rugby_analytics.html#Building-of-the-model)

# Rock, Paper, Scissors (RPS)

Build a machine learning model to predict the outcome
of a rock, paper, scissors game based on past games.

 - [interesting article on the subject](https://towardsdatascience.com/building-a-rock-paper-scissors-ai-948ec424132f)

 - [github](https://github.com/alfischer33/rps-ai)

## Problem Statement

RPS is a game where two players choose, at the same time,
either rock, paper or scissors.

The rules to decide the outcome of the game are as follows:

 - rock beats scissors (r > s)
 - scissors beats paper (s > p)
 - paper beats rock (p > r)
 - if the same option is chosen then the outcome is a draw.
 - the ordering does not matter i.e. (r > s) == (s < r)

Probably the most interesting thing about this game is that
typically many rounds are played against the same player
in quick succession and both players tactics will typically
be influenced by each other (and by self interaction?).
This means that players can quickly become unpredictable.
In other words their autocorellation length is can be very short.
The lack of correlation makes this a tricky problem to be a model
that generalises well. But it is an excellent proving ground
to look at things like concept drift and online learning.

## Outcome Table

The outcome table below is the set of unique inputs and outputs
that are possible for this game.

| Player 1 | Player 2 | Outcome |
| -------- | -------- | ------- |
| rock | rock | draw |
| paper | paper | draw |
| scissors | scissors | draw |
| rock | scissors | P1 wins |
| scissors | paper | P1 wins
| paper | rock | P1 wins |


# Modelling

There are two methods that I want to try to model this game.

 1. Classical machine learning
 2. Bayesian methods

There are multiple questions that we could ask and try and answer but
here I will ask the following specific question:

Suppose we are player 1 and we are playing player 2. Given a history
of games between player 1 and player 2 can we build a model to predict
what player 2 will pick given that player 1 chooses either R, P or S?

## Classical Machine Learning

This method uses supervised learning and involves building a training dataset
that is table of outcomes. Each row in the matrix will be the result
of one game of RPS and we build a classifier to predict what player 2
will play given player 1's choice.

### Training dataset

| game index | player 1 | player 2 | outcome (p1 win/lose/draw) |
| ---------- | -------- | -------- | ------- |
| 1 | r | p | p1 loss |
| 2 | s | p | p1 win |
| 3 | p | p | p1 draw |

Getting a realistic training dataset might be a bit tricky.
Because the whole point is that the choices a human might make
are not entirely random but some how correlated based on many
latent (hidden) factors and the only window into the latent space
is the results of the RPS game.

So, maybe there is an online dataset somewhere?

 - [dataset](https://raw.githubusercontent.com/alfischer33/rps-ai/master/rps-record_dtclf.csv)


### Inference dataset

| game index | player 1 | player 2 | outcome (p1 win/lose/draw) |
| ---------- | -------- | -------- | ------- |
| 1 | r | ?? | ?? |

## Bayesian Methods

In the Bayesian model we describe each players internal decision process
with a probability distribution - a discrete multi-class probability
distribution that provides the probability of picking either R, P or S.

The parameters of this distribution are found by fitting the distribution
to a training dataset. Probably the same dataset as above.

## Online learning

Because the data is streaming in maybe on online learning algorithm is more
appropriate.
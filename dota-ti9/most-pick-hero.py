import pandas as pd
import numpy as np

pd.set_option("display.max_rows", None)

def parse_picks(row):
    return row.split("  ")


def get_picks(df):
    all_picks = [parse_picks(row) for row in df["Picks"]]
    assert [len(all_pick)==5 for all_pick in all_picks]
    all_picks_0 = np.array(all_picks).reshape(-1, )

    all_picks = [parse_picks(row) for row in df["Picks.1"]]
    assert [len(all_pick)==5 for all_pick in all_picks]
    all_picks_1 = np.array(all_picks).reshape(-1, )

    return np.concatenate([all_picks_0, all_picks_1], axis=0)

    


df = pd.read_csv("datdota_The_International_2019.csv")

all_picks = get_picks(df)

all_picks = dict(heroes=all_picks)

picks = pd.DataFrame(all_picks)


print(picks.value_counts())

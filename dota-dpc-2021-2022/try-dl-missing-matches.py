from bs4 import BeautifulSoup
from urllib.request import urlopen
import json
import pandas as pd
import os

df = pd.read_csv("match_ids.csv")

mask_not_parsed = df['state'] != 'parsed'

df_not_parsed = df[mask_not_parsed]


out_dir = "match_data"

for i, match_id in enumerate(range(len(df_not_parsed))):
    match_id = df_not_parsed['match_id'].values[i]
    print(f"{i}, match_id: {match_id}")
    region = df_not_parsed['region'].values[i]
    out_path = os.path.join(out_dir, region)
    url = f"https://www.datdota.com/api/matches/{match_id}"
    html = urlopen(url)
    soup = BeautifulSoup(html,"html.parser")
    json_dict = json.loads(str(soup))
    output_file = os.path.join(out_path, f"{int(match_id)}.json")
    with open(output_file, "w") as f:
        json.dump(json_dict, f, indent=4, sort_keys=True)
# https://developers.cloudflare.com/analytics/graphql-api/tutorials/export-graphql-to-csv

import requests
import json
import pandas as pd
import configparser

config = configparser.ConfigParser()
config.read('/Users/sebastian.khan/personal/.dota-api.ini')
api_token = config['stratz']['key']

url = "https://api.stratz.com/graphql"

def get_cf_graphql(query):
    headers = {"Authorization": f"Bearer {api_token}"}
    r = requests.post(url, json={"query":query}, headers=headers)
    return r
    

query = """{
  matches(ids: [4986461644]) {
    stats {
      pickBans {
        heroId
        isPick
        order
        playerIndex
        team
        wasBannedSuccessfully
      }
      }
    }
}
"""

r = get_cf_graphql(query)
data = json.loads(r.text)

with open('json_data.json', 'w') as outfile:
    json.dump(data, outfile, indent=4)
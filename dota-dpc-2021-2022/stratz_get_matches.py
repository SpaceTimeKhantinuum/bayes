# https://developers.cloudflare.com/analytics/graphql-api/tutorials/export-graphql-to-csv
import stratz_template_query as stq

from string import Template
import requests
import json
import configparser
import os

division_1_dict = {
    'WEU':13738,
    # 'EEU':13709,
    # 'CN':13716,
    'SEA':13747,
    # 'NA':13741,
    # 'SA':13712
}

out_dir = "stratz_match_data_2021_2022_tour_1"
os.makedirs(out_dir, exist_ok=True)

config = configparser.ConfigParser()
config.read('/Users/sebastian.khan/personal/.dota-api.ini')
api_token = config['stratz']['key']

url = "https://api.stratz.com/graphql"

def get_cf_graphql(query):
    headers = {"Authorization": f"Bearer {api_token}"}
    r = requests.post(url, json={"query":query}, headers=headers)
    return r
    

# stratz have a limit on the amount of data (and depth)
# of data you can query
# so we use 'take' and 'skip' to reduce the number of 
# things we ask for but instead spread it out
# over more queries.
# this was mainly one because I found that I couldn't get pickBans
# information for all the games.


n_steps = 20
take = 5
for k,v in division_1_dict.items():
    skip = 0
    all_data = []
    print(f"working region: {k}, with league_id: {v}")

    for i in range(n_steps):
        print(f"\t step: {i}")
        t = Template(stq.all_matches_from_league_template)
        query = t.substitute({'league_id':v, 'take':take, 'skip':skip})
        r = get_cf_graphql(query)
        data = json.loads(r.text)
        matches = data['data']['league']['matches']
        all_data.append(data)

        print(f"found {len(matches)} matches")
        if i==0:
            expected_number_of_matches = data['data']['league']['stats']['matchCount']
            print(f"expected total number of matches: {expected_number_of_matches}")

        if len(matches) == 0:
            print("breaking")
            break

        skip += take

    ms = []
    for d in all_data:
       ms += d['data']['league']['matches']

    data['data']['league']['matches'] = ms

    print("check we have all the matches")
    n_matches_found = len(data['data']['league']['matches'])

    print(f"we have found: {n_matches_found}")

    if n_matches_found < expected_number_of_matches:
       print(">>> you still have more matches to get - increase n_steps") 

    fname = os.path.join(out_dir, f"{k}.json")
    with open(fname, 'w') as outfile:
        json.dump(data, outfile, indent=4)
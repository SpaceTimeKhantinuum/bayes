# %% [markdown]
# # Get match data 
#
# Get all match data for each division 1 region
# using the match IDs from 'get-all-matches.py'
# %%
# imports
from bs4 import BeautifulSoup
from urllib.request import urlopen
import json
import pandas as pd
import os
# %%
out_dir = "match_data"
os.makedirs(out_dir, exist_ok=True)
# %%
df = pd.read_csv("./match_ids.csv", keep_default_na=False)
# need 'keep_default_na=False' because 'region' == "NA"
# get incorrectly interpreted as 'NaN' otherwise...
# %%
regions = df['region'].unique()
print(regions)

print("number of matches in each region so far")
print(df.groupby(by='region')['match_id'].count())
# %%
match_data_dict = {}
failed_cases = []
for region in regions:
    print(region)
    out_path = os.path.join(out_dir, region)
    os.makedirs(out_path, exist_ok=True)
    mask = df['region'] == region
    _df = df.loc[mask]
    for i in range(len(_df)):
        match_id = _df.iloc[i]['match_id']
        state = _df.iloc[i]['state']
        print(f"match_id: {match_id}")
        output_file = os.path.join(out_path, f"{int(match_id)}.json")
        if os.path.exists(output_file):
            print("passing")
            continue
        if state == 'failed':
            print("case failed")
            failed_cases.append(match_id)
            continue
        url = f"https://www.datdota.com/api/matches/{match_id}"
        html = urlopen(url)
        soup = BeautifulSoup(html,"html.parser")
        json_dict = json.loads(str(soup))
        
        with open(output_file, "w") as f:
            json.dump(json_dict, f, indent=4, sort_keys=True)
failed_cases = pd.DataFrame({'match_id':failed_cases})
failed_cases.to_csv("failed_cases.csv", index=False)
# %%

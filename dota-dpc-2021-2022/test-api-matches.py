# %% [markdown]
# Example how to use the datdota api
# to get a specific match data
# 
# %%
from bs4 import BeautifulSoup
from urllib.request import urlopen
import json
# %%
matchid = 6310069641
# %%
url = f"https://www.datdota.com/api/matches/{matchid}"
# %%
html = urlopen(url)
soup = BeautifulSoup(html,"html.parser")
json_dict = json.loads(str(soup))
# %%
print(json_dict.keys())
# %%

# %% [markdown]
# # Get matchIDs
#
# Get all match id's for each division 1 region
# %%
# imports
from bs4 import BeautifulSoup
from urllib.request import urlopen
import json
import pandas as pd
# %%
division_1_dict = {
    'WEU':13738,
    'EEU':13709,
    'CN':13716,
    'SEA':13747,
    'NA':13741,
    'SA':13712
}

# %%
match_ids_dict = {}
dfs = []
for k, v in division_1_dict.items():
    print(k, v)
    url = f"https://www.datdota.com/api/leagues/{v}"
    html = urlopen(url)
    soup = BeautifulSoup(html,"html.parser")
    json_dict = json.loads(str(soup))
    df_ = pd.DataFrame(json_dict['data']['recentGames'])
    df_['region'] = k
    dfs.append(df_)
# %%
df = pd.concat((dfs))
df = df.reset_index(drop=True)
df['match_id'] = df['match_id'].apply(lambda d: int(d))
df['start_date'] = df['start_date'].apply(lambda d: int(d))
# %%
df
# %%
df.to_csv("match_ids.csv", index=False)
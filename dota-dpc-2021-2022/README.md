# Introduction

Analysing DPC 2021-2022

## software

```
conda activate bayes
conda install -c conda-force pymc3==3.8
```

need to downgrade pymc3 to get around a float128 error
[issue](https://github.com/pymc-devs/resources/issues/90)

## datdota league api dictionary


### Division 1

 - WEU
    - https://datdota.com/leagues/13738
    - https://datdota.com/api/leagues/13738
 - EEU
    - https://datdota.com/leagues/13709
    - https://datdota.com/api/leagues/13709
 - CN
    - https://datdota.com/leagues/13716
    - https://datdota.com/api/leagues/13716
 - SEA
    - https://datdota.com/leagues/13747
    - https://datdota.com/api/leagues/13747
 - NA
    - https://datdota.com/leagues/13741
    - https://datdota.com/api/leagues/13741
 - SA
    - https://datdota.com/leagues/13712
    - https://datdota.com/api/leagues/13712

Using this you can get the matchIDs for matches in each
league. Then download each match data separately.

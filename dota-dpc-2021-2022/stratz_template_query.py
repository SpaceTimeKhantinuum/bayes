all_matches_from_league_template = """
{
  league(id: ${league_id}) {
    id
    region
    startDateTime
    endDateTime
    prizePool
    basePrizePool
    stats {
      matchCount
      radiantWinMatchCount
      averageMatchDurationSeconds
    }
    matches(request: {take: ${take}, skip: ${skip}}) {
      id
      didRadiantWin
      startDateTime
      endDateTime
      durationSeconds
      firstBloodTime
      clusterId
      replaySalt
      direTeamId
      radiantTeamId
      direTeam {
        id
        name
      }
      radiantTeam {
        id
        name
      }
      series {
        id
        type
        teamOneWinCount
        teamTwoWinCount
        teamOne {
          id
          name
        }
        teamTwo {
          id
          name
        }
        winningTeamId
        matches {
          id
        }
      }
      stats {
        radiantKills
        direKills
        pickBans {
          heroId
          isPick
          order
          playerIndex
          team
          wasBannedSuccessfully
        }
      }
      players {
        matchId
        playerSlot
        steamAccountId
        isRadiant
        isVictory
        heroId
        gameVersionId
        kills
        deaths
        assists
        leaverStatus
        numLastHits
        numDenies
        goldPerMinute
        networth
        experiencePerMinute
        level
        gold
        goldSpent
        heroDamage
        towerDamage
        heroHealing
        partyId
        isRandom
        lane
        streakPrediction
        intentionalFeeding
        role
        roleBasic
        imp
        award
        item0Id
        item1Id
        item2Id
        item3Id
        item4Id
        item5Id
        backpack0Id
        backpack1Id
        backpack2Id
        neutral0Id
        behavior
      }
    }
  }
}
"""